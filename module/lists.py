def list_primer(lists):
    return lists[0]


def composite(number_of_list):
    for i in number_of_list:
        if i % 2 == 0:
            return i


def even(a):
    new_list = [2 * i for i in range(a)]
    return new_list, sum(new_list)


languages = list_primer([('English', 'Albanian'), 'Gujarati', 'Hindi', 'Romanian', 'Spanish'])
print(languages)
i_number = composite([1, 4, 5])
print(f"{i_number} is composite\n")
new_list = even(3)
print(new_list)
