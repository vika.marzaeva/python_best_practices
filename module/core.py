x = 10
printer = 'HP'
print(f"I just printed {x} pages to the printer {printer}")

age, city = 29, 'Moscow'
print(age, city)

person = {'city': 'New York', 'age': 30}
print(person['city'])
print(person.keys())
print(person.values())
print(person.get('age'))

# split()
s = 'No. Okay. Why?'.split('.')
print(s)

# join()
j = ", ".join(['red','green','blue'])
print(j)
print("We need a 'chaperone'")
